SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
create view HumanResources.EmployeeList (EmployeeID,FamilyName,GivenName) 
as
select BusinessEntityID,LastName,FirstName from Person.Person;
GO
