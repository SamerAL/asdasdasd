SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
create view v_HireDate (EmployeeID, FirstName, LastName, HireDate)
as
select p.BusinessEntityID,FirstName,Lastname, e.HireDate
from Person.Person as p
join HumanResources.Employee as e
on p.BusinessEntityID=e.BusinessEntityID;
GO
