SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
--alter procedure to return null values
CREATE proc Production.GetProductAndModelsByColor
@Color nvarchar(15)
as
begin
select p.ProductID,p.Name,p.Size,p.ListPrice
from Production.Product as p
where (p.Color = @Color) or (p.Color is null and @Color is null)
 order by p.ProductID;

select p.ProductID, pm.ProductModelID,pm.Name as ModelName
from Production.Product as p
inner join Production.ProductModel as pm
on p.ProductModelID = pm.ProductModelID
where p.Color=@Color order by p.ProductID, pm.ProductModelID
end;
GO
