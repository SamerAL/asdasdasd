SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PhoneLog] (
		[PhoneLogID]            [int] IDENTITY(1, 1) NOT NULL,
		[LogRecorder]           [datetime2](7) NOT NULL,
		[PhoneNumberCalled]     [nvarchar](1000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[CallDuration]          [int] NOT NULL,
		CONSTRAINT [PK__PhoneLog__A8E44E3253F45F03]
		PRIMARY KEY
		CLUSTERED
		([PhoneLogID])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PhoneLog] SET (LOCK_ESCALATION = TABLE)
GO
